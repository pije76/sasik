<?php
ob_start();
set_time_limit(0); //keep running forever

require('vendor/autoload.php');
require_once 'src/whatsprot.class.php';

use Aws\S3\S3Client;

$username = getenv('USERNAME')?: die('No "USERNAME" config var in found in env!');
$password = getenv('PASSWORD')?: die('No "PASSWORD" config var in found in env!');
$nickname = getenv('NICK_NAME')?: die('No "NICK_NAME" config var in found in env!');

$awsAccessKey = getenv('AWS_ACCESS_KEY_ID')?: die('No "AWS_ACCESS_KEY_ID" config var in found in env!');
$awsSecretKey = getenv('AWS_SECRET_ACCESS_KEY')?: die('No "AWS_SECRET_ACCESS_KEY" config var in found in env!');
$bucket = getenv('S3_BUCKET')?: die('No "S3_BUCKET" config var in found in env!');

$s3 = S3Client::factory(array(
  'key' => $awsAccessKey,
  'secret' => $awsSecretKey
));

function onGetImage($mynumber, $from, $id, $type, $t, $name, $size, $url, $file, $mimetype, $filehash, $width, $height, $preview){
  $from_id = explode('@', $from)[0];

  $result = $GLOBALS['s3']->putObject(array(
    'Bucket' => $GLOBALS['bucket'],
    'Key' => $from_id . '/' . $filehash,
    'Body'   => file_get_contents($url)
  ));
}

$w = new WhatsProt($username, 0, $nickname, true);
$w->eventManager()->bind("onGetImage", "onGetImage");
$w->connect();
$w->loginWithPassword($password);

while (true) {
  $w->pollMessage($autoReceipt = true);
  $messages = $w->getMessages();
  sleep(1);
}

ob_flush();
?>
